package com.example.kimsoerhrd.retrofitdemo.entity.respone;


import com.example.kimsoerhrd.retrofitdemo.entity.Category;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class CategoryRespone {

    @SerializedName("data")
    private List<Category> data;
    @SerializedName("msg")
    private String msg;
    @SerializedName("status")
    private boolean status;
    @SerializedName("code")
    private String code;

    public List<Category> getData() {
        return data;
    }

    public void setData(List<Category> data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "CategoryRespone{" +
                "data=" + data +
                ", msg='" + msg + '\'' +
                ", status=" + status +
                ", code='" + code + '\'' +
                '}';
    }
}
