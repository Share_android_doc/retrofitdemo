package com.example.kimsoerhrd.retrofitdemo.repository;


import com.example.kimsoerhrd.retrofitdemo.entity.respone.CategoryRespone;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface KnongdaiService {
    interface CategoryService{
        @GET("api/v1/categories")
        Call<CategoryRespone> getCategories();

        @GET("api/v1/categories/{id}")
        Call<CategoryRespone> getCategory(@Path("id") int id);

    }
}
