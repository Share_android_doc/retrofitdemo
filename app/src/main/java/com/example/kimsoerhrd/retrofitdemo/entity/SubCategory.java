package com.example.kimsoerhrd.retrofitdemo.entity;

import com.google.gson.annotations.SerializedName;

public class SubCategory {


    @SerializedName("total_url")
    private int total_url;
    @SerializedName("des")
    private String des;
    @SerializedName("cate_name")
    private String cate_name;
    @SerializedName("status")
    private boolean status;
    @SerializedName("id")
    private int id;

    public int getTotal_url() {
        return total_url;
    }

    public void setTotal_url(int total_url) {
        this.total_url = total_url;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getCate_name() {
        return cate_name;
    }

    public void setCate_name(String cate_name) {
        this.cate_name = cate_name;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "SubCategory{" +
                "total_url=" + total_url +
                ", des='" + des + '\'' +
                ", cate_name='" + cate_name + '\'' +
                ", status=" + status +
                ", id=" + id +
                '}';
    }
}
