package com.example.kimsoerhrd.retrofitdemo.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Category {
    @SerializedName("sub_cate")
    private List<SubCategory> sub_cate;
    @SerializedName("icon_name")
    private String icon_name;
    @SerializedName("des")
    private String des;
    @SerializedName("cate_name")
    private String cate_name;
    @SerializedName("status")
    private boolean status;
    @SerializedName("id")
    private int id;

    public List<SubCategory> getSub_cate() {
        return sub_cate;
    }

    public void setSub_cate(List<SubCategory> sub_cate) {
        this.sub_cate = sub_cate;
    }

    public String getIcon_name() {
        return icon_name;
    }

    public void setIcon_name(String icon_name) {
        this.icon_name = icon_name;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getCate_name() {
        return cate_name;
    }

    public void setCate_name(String cate_name) {
        this.cate_name = cate_name;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Category{" +
                "sub_cate=" + sub_cate +
                ", icon_name='" + icon_name + '\'' +
                ", des='" + des + '\'' +
                ", cate_name='" + cate_name + '\'' +
                ", status=" + status +
                ", id=" + id +
                '}';
    }
}
