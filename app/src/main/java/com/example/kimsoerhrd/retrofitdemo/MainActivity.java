package com.example.kimsoerhrd.retrofitdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kimsoerhrd.retrofitdemo.entity.Category;
import com.example.kimsoerhrd.retrofitdemo.entity.respone.CategoryRespone;
import com.example.kimsoerhrd.retrofitdemo.repository.KnongdaiService;
import com.example.kimsoerhrd.retrofitdemo.repository.network.ServiceGenerator;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {

   KnongdaiService.CategoryService categoryService;
   TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.tvResult);
        createService();


    }

    private void createService() {
        categoryService = ServiceGenerator.CreateService(KnongdaiService.CategoryService.class);
    }

    public void onGetCategory(View view) {
        getCategory();
    }

    private void getCategory() {
        Call<CategoryRespone> call = categoryService.getCategories();
        call.enqueue(new Callback<CategoryRespone>() {
            @Override
            public void onResponse(Call<CategoryRespone> call, Response<CategoryRespone> response) {

                try{
                    CategoryRespone categoryRespone = response.body();
                    List<Category>  categories = categoryRespone.getData();
                    textView.setText(categories.get(1).toString());
                    Log.e("Category","MyCat=>"+categories.get(0).toString());
                }catch (Exception e){
                    Log.e("error=>", "data error");
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<CategoryRespone> call, Throwable t) {

                Log.e("Error","data-error"+t.getMessage());
               // Toast.makeText(getApplicationContext(),t.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
